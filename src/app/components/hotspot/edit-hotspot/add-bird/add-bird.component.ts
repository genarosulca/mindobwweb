import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import { DataApiService } from '../../../../services/data-api.service';
import { BirdModel } from '../../../../models/bird/bird';
import { HotspotService } from '../../services/hotspot.service';
import { ToastrService } from 'ngx-toastr';
import { startWith, map } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { LanguageService } from '../../../../services/language.service';

@Component({
    selector: 'add-bird-modal',
    templateUrl: './add-bird.component.html',
    styleUrls: ['./add-bird.component.css']
})
export class AddBirdComponent implements OnInit {

    constructor(public hotspotService: HotspotService, private toastr: ToastrService, public languageService:LanguageService) { }

    ngOnInit() { }

    isAvailable(id: string) {
        let exist: string = this.hotspotService.currentHotspot.birdColRef.find(bird => bird == id);
        return exist == undefined ? false : true;
    }

    add(selectedBird: BirdModel): void {
        this.hotspotService.currentHotspot.birdColRef.push(selectedBird.id);
        this.hotspotService.currentBirdCol.push(selectedBird);
        this.toastr.success(`${selectedBird.esInfo.scientificName} ${this.languageService.content.ADDED}`);
    }

    remove(selectedBird: BirdModel): void {
        for (var i = 0; i < this.hotspotService.currentHotspot.birdColRef.length; i++) {
            if (this.hotspotService.currentHotspot.birdColRef[i] == selectedBird.id) {
                this.hotspotService.currentHotspot.birdColRef.splice(i, 1);
            }
        }

        for (var i = 0; i < this.hotspotService.currentBirdCol.length; i++) {
            if (this.hotspotService.currentBirdCol[i].id == selectedBird.id) {
                this.hotspotService.currentBirdCol.splice(i, 1);
            }
        }

        this.toastr.error(`${selectedBird.esInfo.scientificName} ${this.languageService.content.REMOVED}`);
    }
}