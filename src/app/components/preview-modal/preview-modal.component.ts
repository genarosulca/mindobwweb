import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import * as $ from 'jquery';
import { BirdfamiliesService } from '../birdfamilies/service/birdfamilies.service';
import { PreviewModalService } from './preview-modal.service';

@Component({
    selector: 'app-preview-modal',
    templateUrl: './preview-modal.component.html',
    styleUrls: ['./preview-modal.component.css']
})
export class PreviewModalComponent  {


    constructor(public previewModalService:PreviewModalService) {
        
    }

    // ngOnInit() {
    //     // let mod = this.previewModal as NgbModalWindow;
    //     // mod.ngOnInit.
    //     setTimeout(() => {
    //         this.activate()
    //     }, 1000);
    //     // console.log('this.previewModal', mod)
    // }

    // ngOnDestroy() {
    //     // console.log('DESTRUYO')
    //     // this.familiesService.previewUrl = null;
    //     // this.familiesService.previewType = null;
    //     // let activa:NgbActiveModal = this.previewModal;

    // }


}
