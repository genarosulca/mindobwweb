import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class NavigationService {

    public homeOpt: HTMLElement;
    public hotspotOpt: HTMLElement;
    public trailsOpt: HTMLElement;
    public familiesOpt: HTMLElement;
    public birdsOpt: HTMLElement;

    constructor() {
        
    }

    init(){
        this.homeOpt = document.getElementById('homeOpt') as HTMLElement;
        this.hotspotOpt = document.getElementById('hotspotOpt') as HTMLElement;
        this.trailsOpt = document.getElementById('trailsOpt') as HTMLElement;
        this.familiesOpt = document.getElementById('familiesOpt') as HTMLElement;
        this.birdsOpt = document.getElementById('birdsOpt') as HTMLElement;
    }

    deactivateAllSections(){
        if(this.homeOpt != null){
            this.homeOpt.className = '';
            this.hotspotOpt.className = '';
            this.trailsOpt.className = '';
            this.familiesOpt.className = '';
            this.birdsOpt.className = '';
        }
    }

    activateSection(sectionNumber:number){
        this.init();
        this.deactivateAllSections();
        if(sectionNumber == 0){
            this.homeOpt.className = 'active';
        }
        if(sectionNumber == 1){
            this.hotspotOpt.className = 'active';
        }
        if(sectionNumber == 2){
            this.trailsOpt.className = 'active';
        }
        if(sectionNumber == 3){
            this.familiesOpt.className = 'active';
        }
        if(sectionNumber == 4){
            this.birdsOpt.className = 'active';
        }
    }
}
